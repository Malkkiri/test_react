class SLotItemData {
    // TODO: need state, not string
    // private _slotState: State = State.Empty
    private _slotState: string = "-"
    private _slotID: number = 0

    constructor(slotID: number) {
        this._slotID = slotID;
    }

    get slotState(): string {
        return this._slotState;
    }

    set slotState(value: string) {
        this._slotState = value;
    }

    get slotID(): number {
        return this._slotID;
    }

    get isEmpty(): boolean {
        return this._slotState === "-"
    }
}


export default SLotItemData
