import FieldModel from "./model/FieldModel";
import SLotItemData from "./model/SLotItemData";

class GameController {
    private model: FieldModel
    //TODO: need enum
    private curItem: string = "X";
    //TODO: insert view ?
    // private holder?: SlotHolder
    constructor() {
        // this.holder = value
        this.model = new FieldModel()
    }

    public onChooseItem(id: number): void {
        let slot: SLotItemData = this.model.getSlotById(id)
        if (this.model.validateEmptyItem(slot)) {
            this.model.setSlotValue(id, this.curItem)
            this.setNextState();
        } else {
            console.warn("not empty ", slot.slotState)
        }
    }

    public getItems(): SLotItemData[] {
        return this.model.getSlots()
    }

    setNextState(): void {
        this.curItem = this.curItem === 'X' ? 'O' : 'X'
    }
}

export default GameController