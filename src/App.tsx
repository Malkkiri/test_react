import React from 'react';
import './App.css';
import GameController from "./GameController";
import SlotHolder from "./componets/SlotHolder";

class App extends React.Component {
    private controller?: GameController

    constructor(props: any) {
        super(props);
        // TODO Gleb: можно создать компонент и прокинуть его в контролер, а там принудительно обновлять и сетить данные?
        // SlotHolder view = new SlotHolder(props)
        this.controller = new GameController()
    }

    render() {
        return (
            <div>
                <SlotHolder
                    // @ts-ignore
                    items={this.controller?.getItems()}
                    onClick={id => {
                        this.controller?.onChooseItem(id)
                        // TODO Gleb: обновление вьюхи после изменения модели
                        this.forceUpdate()
                    }}
                />
            </div>
        )
    }
}

export default App;
