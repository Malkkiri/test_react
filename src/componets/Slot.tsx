import React from "react";
import SLotItemData from "../model/SLotItemData";

interface SlotState {
    items: SLotItemData;

    onClick(id: number): void
}

class Slot extends React.Component<SlotState, {}> {
    private data: SLotItemData

    constructor(props: any) {
        super(props);
        this.data = props.item
    }

    onChoose(event: React.MouseEvent, id: number) {
        this.props.onClick(id)
    }

    render() {
        return (
            <div className="slot_1"
                 onClick={event => this.onChoose(event, this.data.slotID)}
            >{this.data.slotState}</div>
        )
    }
}

export default Slot
