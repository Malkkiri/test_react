import React from "react";
import Slot from "./Slot";
import SLotItemData from "../model/SLotItemData";

interface ParentState {
    items: SLotItemData[];

    onClick(id: number): void
}

class SlotHolder extends React.Component<ParentState, {}> {
    private slotsData?: Array<SLotItemData>
    private choose?: Function

    constructor(props: any) {
        super(props);
        this.slotsData = props.items
        this.choose = props.onClick
    }

    render() {
        // TODO: remove duplicate logic
        let row_1: SLotItemData[] = new Array<SLotItemData>()
        for (let i: number = 0; i < 3; i++) {
            // @ts-ignore
            row_1.push(this.slotsData[i])
        }
        let row_2: SLotItemData[] = new Array<SLotItemData>()
        for (let i: number = 3; i < 6; i++) {
            // @ts-ignore
            row_2.push(this.slotsData[i])
        }
        let row_3: SLotItemData[] = new Array<SLotItemData>()
        for (let i: number = 6; i < 9; i++) {
            // @ts-ignore
            row_3.push(this.slotsData[i])
        }
        // TODO: remove duplicate logic
        return (
            <div className="grid">
                <div>
                    {row_1.map((slot, i) => {
                        return (
                            <Slot
                                // @ts-ignore
                                item={slot}
                                onClick={id => {
                                    this.props.onClick(id)
                                }}
                                //TODO Gleb: хз зачем key, без него сыпет варнинг про "key"
                                key={i}
                            />)
                    })}
                </div>
                <div>
                    {row_2.map((slot, i) => {
                        return (
                            <Slot
                                // @ts-ignore
                                item={slot}
                                onClick={id => {
                                    this.props.onClick(id)
                                }}
                                //TODO Gleb: хз зачем key, без него сыпет варнинг про "key"
                                key={i}
                            />)
                    })}
                </div>
                <div>
                    {row_3.map((slot, i) => {
                        return (
                            <Slot
                                // @ts-ignore
                                item={slot}
                                onClick={id => {
                                    this.props.onClick(id)
                                }}
                                //TODO Gleb: хз зачем key, без него сыпет варнинг про "key"
                                key={i}
                            />)
                    })}
                </div>
            </div>
        )
    }
}

export default SlotHolder
