import SLotItemData from "./SLotItemData";
// enum State {
//     Empty,
//     ItemX,
//     ItemO,
// }

class FieldModel {
    private row: number = 0
    private column: number = 0
    private _slots: SLotItemData[] = new Array<SLotItemData>()

    constructor() {
        this.setFieldSize(3, 3)
        this.generateField()
    }

    public validateEmptyItem(slot: SLotItemData): boolean {
        return slot.isEmpty
    }

    public getSlotById(id: number): SLotItemData {
        for (let i = 0; i < this._slots.length; i++) {
            if (this._slots[i].slotID === id) {
                return this._slots[i]
            }
        }
        return this._slots[0]
    }

    public setSlotValue(id: number, value: string) {
        this.getSlotById(id).slotState = value
    }

    setFieldSize(rowVal: number, colVal: number): void {
        this.row = rowVal;
        this.column = colVal;
    }

    generateField(): void {
        let idCount: number = 1;
        for (let i = 0; i < this.row; i++) {
            for (let j = 0; j < this.column; j++) {
                this._slots.push(new SLotItemData(idCount))
                idCount++
            }
        }
    }

    getSlots(): SLotItemData[] {
        return this._slots;
    }
}

export default FieldModel